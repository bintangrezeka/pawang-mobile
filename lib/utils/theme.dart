import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// Colors
const Color kPurple = Color(0xFF6C72CB);
const Color kBlack = Color(0xFF001833);
const Color kWarning = Color(0xFFFAD202);
const Color kSuccess = Color(0xFF64BC26);

// Padding
const double kDefaultPadding = 16.0;
const double kDefaultPadding2x = 32.0;

// Border Radius
const double kDefaultBorderRadius = 12.0;

// Fonts
TextStyle kOpenSans = GoogleFonts.openSans();

// Font Weight
FontWeight light = FontWeight.w300;
FontWeight medium = FontWeight.w500;
FontWeight semibold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
