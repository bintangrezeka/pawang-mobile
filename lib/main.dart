import 'package:flutter/material.dart';
import 'package:pawang/views/landing_screen.dart';
import 'package:pawang/views/riwayat_screen.dart';
import 'package:pawang/views/struk/scan_screen.dart';
import 'package:pawang/views/struk/validasi_scan_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        debugShowCheckedModeBanner: false,
        routes: {
          LandingScreen.routeName: (context) => const LandingScreen(),
          RiwayatScreen.routeName: (context) => RiwayatScreen(),
          ScanScreen.routeName: (context) => const ScanScreen(),
          ValidasiScanScreen.routeName: (context) => const ValidasiScanScreen(),
        });
  }
}
