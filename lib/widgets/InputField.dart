import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pawang/utils/theme.dart';

class InputField extends StatelessWidget {
  const InputField(
      {Key? key,
      required this.inputLabel,
      required this.inputController,
      this.onTap,
      this.readOnly = false,
      this.keyboardType,
      this.suffixIcon})
      : super(key: key);
  final String inputLabel;
  final TextEditingController inputController;
  final Function()? onTap;
  final bool readOnly;
  final Widget? suffixIcon;
  final TextInputType? keyboardType;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text(
          inputLabel,
          style: kOpenSans.copyWith(fontWeight: bold, color: kBlack),
        ),
        SizedBox(
          height: 10,
        ),
        TextField(
          controller: inputController,
          keyboardType: keyboardType,
          decoration: InputDecoration(
            fillColor: const Color(0xFFF5F5F5),
            filled: true,
            suffixIcon: suffixIcon,
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(kDefaultBorderRadius),
                borderSide: BorderSide.none),
          ),
          readOnly: readOnly,
          onTap: onTap ?? () {},
        ),
      ]),
    );
  }
}
