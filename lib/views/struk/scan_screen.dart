import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:image_picker/image_picker.dart';
import 'package:pawang/utils/theme.dart';
import 'package:pawang/views/riwayat_screen.dart';
import 'package:pawang/views/struk/validasi_scan_screen.dart';

class ScanScreen extends StatefulWidget {
  static const String routeName = '/scan-struk';
  const ScanScreen({Key? key}) : super(key: key);

  @override
  State<ScanScreen> createState() => _ScanScreenState();
}

class _ScanScreenState extends State<ScanScreen> {
  XFile? _image;

  final ImagePicker _picker = ImagePicker();

  Future getImage() async {
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);

    if (image != null) {
      setState(() {
        _image = image;
      });

      final inputImage = InputImage.fromFilePath(image.path);
      final textDetector = GoogleMlKit.vision.textDetector();
      final RecognisedText recognisedText =
          await textDetector.processImage(inputImage);

      final findTotal = <double>[];
      final findIndex = <int>[];
      double total_belanja = 0;
      double temp;
      int counter = 0;
      bool found = false;

      for (TextBlock block in recognisedText.blocks) {
        for (TextLine line in block.lines) {
          print('text : ${line.text} | corner : ${line.cornerPoints}');
          if (line.text.contains(RegExp(r'[A-Z]')) ||
              line.text.contains(RegExp(r'[a-z]')) ||
              line.text.contains(RegExp(r'=')) &&
                  !line.text.contains(RegExp(r'Rp')) &&
                  !line.text.contains(RegExp(r'RP'))) {
            counter++;
            if (line.text.contains(RegExp(r'total')) ||
                line.text.contains(RegExp(r'TOTAL')) &&
                    line.text.compareTo('Subtotal') != 0 &&
                    line.text.compareTo('SUBTOTAL') != 0) {
              findIndex.add(counter - 1);
              found = true;
            }
          } else {
            if (found) {
              String a = line.text.replaceAll(RegExp('[^A-Za-z0-9]'), '');
              bool cond1 =
                  a.contains('Rp') || a.contains('Rp.') || a.contains('Rp. ');
              bool cond2 =
                  a.contains('RP') || a.contains('RP.') || a.contains('RP. ');
              if (a.contains(' ')) {
                a = a.replaceAll(RegExp(r' '), '');
              }
              if (cond1) {
                a = a.replaceAll(RegExp(r'Rp'), '');
              } else if (cond2) {
                a = a.replaceAll(RegExp(r'RP'), '');
              }
              if (a.compareTo('') != 0) {
                findTotal.add(double.parse(a));
              }
            }
          }
        }
        counter = 0;
      }
      // print('pjg findIndex : ${findIndex.length}');
      // for (double i in findTotal) {
      //   print('i[${counter + 1}] : ${i}');
      //   counter++;
      // }
      // flag = findIndex[findIndex.length - 1];
      // catch the total
      temp = findTotal[0];
      for (double i in findTotal) {
        if (i >= temp) {
          total_belanja = i;
        } else {
          temp = i;
        }
      }
      // total_belanja = findTotal[findIndex.length - 1];
      print('total belanja : ${total_belanja}');
      // print('found : ${found}');
      // print('panjang list e : ${j}');
      // print('flag : ${flag}');

      Navigator.pushNamed(context, ValidasiScanScreen.routeName,
          arguments: total_belanja);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 33.0, horizontal: 32.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.all(Colors.transparent),
                        foregroundColor:
                            MaterialStateProperty.all(Colors.transparent),
                        shadowColor:
                            MaterialStateProperty.all(Colors.transparent),
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        width: 32,
                        height: 32,
                        padding: EdgeInsets.all(6),
                        decoration: BoxDecoration(
                          border: Border.all(color: kPurple),
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: InkWell(
                          child: SvgPicture.asset(
                            'assets/images/back_btn.svg',
                            fit: BoxFit.cover,
                          ),
                          onTap: () => {
                            Navigator.pushNamed(
                                context, RiwayatScreen.routeName),
                          },
                        ),
                      ),
                    ),
                    Text(
                      "Scan Struk",
                      style: kOpenSans.copyWith(
                          fontSize: 16, fontWeight: bold, color: kBlack),
                    ),
                    Container(
                      width: 32,
                      height: 32,
                      padding: EdgeInsets.all(6),
                    ),
                  ],
                ),
                SizedBox(
                  height: 33,
                ),
                Text(
                  "Mohon atur posisi struk agar dapat terdeteksi",
                  style: kOpenSans.copyWith(color: kBlack),
                ),
                SizedBox(
                  height: 28,
                ),
                Flexible(
                  fit: FlexFit.loose,
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: double.infinity,
                          height: MediaQuery.of(context).size.height * 0.73,
                          child: Center(
                            child: _image != null
                                ? Image.file(File(_image!.path))
                                : Text(
                                    "Silahkan Inputkan Struk Terlebih Dahulu"),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: [
                            Flexible(
                              fit: FlexFit.tight,
                              flex: 1,
                              child: OutlinedButton(
                                  onPressed: getImage,
                                  style: OutlinedButton.styleFrom(
                                      padding:
                                          EdgeInsets.symmetric(vertical: 12),
                                      side: BorderSide(color: kPurple),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(12))),
                                  child: Text(
                                    "Pilih Dari Galeri",
                                    style: kOpenSans.copyWith(
                                        fontSize: 16,
                                        fontWeight: bold,
                                        color: kPurple),
                                  )),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
