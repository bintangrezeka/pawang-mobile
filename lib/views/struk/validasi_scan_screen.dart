import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:pawang/model/PengeluaranModel.dart';
import 'package:pawang/services/PengeluaranService.dart';
import 'package:pawang/utils/theme.dart';
import 'package:pawang/views/riwayat_screen.dart';
import 'package:pawang/widgets/InputField.dart';

class ValidasiScanScreen extends StatefulWidget {
  static const String routeName = '/validasi-scan';
  const ValidasiScanScreen({Key? key}) : super(key: key);

  @override
  State<ValidasiScanScreen> createState() => _ValidasiScanScreenState();
}

class _ValidasiScanScreenState extends State<ValidasiScanScreen> {
  final TextEditingController nama_pengeluaran = TextEditingController();
  final TextEditingController nominal_pengeluaran = TextEditingController();
  final TextEditingController kategori_pengeluaran = TextEditingController();
  final TextEditingController tanggal_pengeluaran = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final arguments_nominal =
        ModalRoute.of(context)!.settings.arguments as double;
    nominal_pengeluaran.text = arguments_nominal.toString();
    DateTime tempDate = DateTime.now();

    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 33.0, horizontal: 32.0),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all(Colors.transparent),
                      foregroundColor:
                          MaterialStateProperty.all(Colors.transparent),
                      shadowColor:
                          MaterialStateProperty.all(Colors.transparent),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      width: 32,
                      height: 32,
                      padding: EdgeInsets.all(6),
                      decoration: BoxDecoration(
                        border: Border.all(color: kPurple),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: InkWell(
                        child: SvgPicture.asset(
                          'assets/images/back_btn.svg',
                          fit: BoxFit.cover,
                        ),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                    ),
                  ),
                  Text(
                    "Validasi Scan Struk",
                    style: kOpenSans.copyWith(
                        fontSize: 16, fontWeight: bold, color: kBlack),
                  ),
                  Container(
                    width: 32,
                    height: 32,
                    padding: EdgeInsets.all(6),
                  ),
                ],
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "Mohon untuk melakukan validasi untuk menghindari kesalahan",
                style: kOpenSans.copyWith(fontSize: 12),
              ),
              SizedBox(
                height: 30,
              ),
              Container(
                margin: EdgeInsets.only(bottom: 20),
                child: InputField(
                  inputLabel: "Nama Pengeluaran",
                  inputController: nama_pengeluaran,
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 20),
                child: InputField(
                  keyboardType: TextInputType.number,
                  inputLabel: "Nominal",
                  inputController: nominal_pengeluaran,
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 20),
                child: InputField(
                  inputLabel: "Kategori",
                  inputController: kategori_pengeluaran,
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 20),
                child: InputField(
                  inputLabel: "Tanggal",
                  keyboardType: TextInputType.none,
                  inputController: tanggal_pengeluaran,
                  readOnly: true,
                  suffixIcon: Icon(Icons.calendar_today),
                  onTap: () async {
                    DateTime? pickedDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(
                            2000), //DateTime.now() - not to allow to choose before today.
                        lastDate: DateTime(2101));

                    if (pickedDate != null) {
                      String formated =
                          DateFormat('dd-MM-yyyy').format(pickedDate);

                      print(formated);

                      setState(() {
                        tempDate = pickedDate;
                        tanggal_pengeluaran.text = formated;
                      });
                    } else {
                      print("Date is not selected");
                    }
                  },
                ),
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                        style: ButtonStyle(
                          padding: MaterialStateProperty.all(
                            EdgeInsets.symmetric(vertical: 15),
                          ),
                          backgroundColor: MaterialStateProperty.all(kPurple),
                          shape: MaterialStateProperty.all(
                            RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(kDefaultBorderRadius),
                            ),
                          ),
                        ),
                        onPressed: () async {
                          var data = PengeluaranModel(
                              nama_pengeluaran: nama_pengeluaran.text,
                              nominal_pengeluaran:
                                  double.parse(nominal_pengeluaran.text),
                              kategori_pengeluaran: kategori_pengeluaran.text,
                              tanggal_pengeluaran: tempDate.toString());

                          await PengeluaranService().create(data).then(
                              (value) => Navigator.pushReplacementNamed(
                                  context, RiwayatScreen.routeName));
                        },
                        child: Text(
                          "Simpan Pengeluaran",
                          style: kOpenSans.copyWith(
                            fontSize: 16,
                            fontWeight: bold,
                          ),
                        )),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
