import 'package:flutter/material.dart';
import 'package:pawang/model/PengeluaranModel.dart';
import 'package:pawang/services/PengeluaranService.dart';
import 'package:pawang/utils/theme.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:pawang/views/struk/scan_screen.dart';

class RiwayatScreen extends StatefulWidget {
  static const String routeName = '/riwayat';
  RiwayatScreen({Key? key}) : super(key: key);

  @override
  State<RiwayatScreen> createState() => _RiwayatScreenState();
}

class _RiwayatScreenState extends State<RiwayatScreen> {
  late Future<List<PengeluaranModel>> dataPengeluaran;

  @override
  void initState() {
    super.initState();
    updateListView();
  }

  void updateListView() {
    setState(() {
      dataPengeluaran = PengeluaranService().read();
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, ScanScreen.routeName);
        },
        child: SvgPicture.asset('assets/images/scan_btn.svg'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(top: 30, left: 32, right: 32),
          child: Column(
            children: [
              Center(
                child: Text(
                  "Riwayat",
                  style: kOpenSans.copyWith(
                      color: kBlack, fontWeight: bold, fontSize: 16),
                ),
              ),
              SizedBox(
                height: 31,
              ),
              Expanded(
                  child: FutureBuilder(
                future: dataPengeluaran,
                builder: (BuildContext context,
                    AsyncSnapshot<List<PengeluaranModel>> snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          margin: EdgeInsets.only(bottom: 18),
                          padding: EdgeInsets.symmetric(
                              vertical: 12, horizontal: 21),
                          width: 296,
                          height: 106,
                          decoration: BoxDecoration(
                              color: Color(0xFFF0F0F0),
                              borderRadius: BorderRadius.circular(6)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Expanded(
                                child: Text(
                                  snapshot.data![index].nama_pengeluaran,
                                  style: kOpenSans.copyWith(
                                      fontSize: 12, fontWeight: bold),
                                ),
                              ),
                              Expanded(
                                child: Row(
                                  children: [
                                    Text(
                                      "Nominal : ",
                                      style: kOpenSans.copyWith(
                                          fontSize: 12, fontWeight: semibold),
                                    ),
                                    SizedBox(
                                      width: 3,
                                    ),
                                    Text(
                                      "- Rp. ${snapshot.data![index].nominal_pengeluaran.toString()}",
                                      style: kOpenSans.copyWith(
                                          fontSize: 10,
                                          fontWeight: semibold,
                                          color: kWarning),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Row(
                                  children: [
                                    Text(
                                      "Kategori : ",
                                      style: kOpenSans.copyWith(
                                          fontSize: 12, fontWeight: semibold),
                                    ),
                                    SizedBox(
                                      width: 3,
                                    ),
                                    Text(
                                      snapshot
                                          .data![index].kategori_pengeluaran,
                                      style: kOpenSans.copyWith(fontSize: 10),
                                    ),
                                  ],
                                ),
                              ),
                              Expanded(
                                child: Row(
                                  children: [
                                    Text(
                                      "Waktu : ",
                                      style: kOpenSans.copyWith(
                                          fontSize: 12, fontWeight: semibold),
                                    ),
                                    SizedBox(
                                      width: 3,
                                    ),
                                    Text(
                                      snapshot.data![index].tanggal_pengeluaran,
                                      style: kOpenSans.copyWith(fontSize: 10),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                      itemCount: snapshot.data?.length,
                    );
                  } else {
                    return const Center(child: CircularProgressIndicator());
                  }
                },
              ))
            ],
          ),
        ),
      ),
    );
  }
}
